﻿using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test21_Visualizar_datos_Empresa
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
        static login login = new login();
        Visualización_Datos_Empresa vis = new Visualización_Datos_Empresa();
        /*
        * CP01-21	buscar usuario (Empresa) busqueda de usuario Creado
        */
        [Test]
#pragma warning disable CS1591
        public void Test21_Visualizar_datos_Empresa_()
        {
           
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                vis.VerDatosUsuarioEmpresaBOClaroreado(driver, lista, DatosBO._011902_usuarioAdminEmpresa, LocalizadoresBO._013201_opciónBusqEmpresa, DatosBO._012001_SigP1);
           
        }

    }
}