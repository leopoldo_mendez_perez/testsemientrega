﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
#pragma warning disable CS1591
    public class Test18_Eliminar_Usuario_Admin
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();

        static login login = new login();
        Eliminación_Usuario_Administrador eli = new Eliminación_Usuario_Administrador();
        /*
        * CP01-18 Usuario (Administrador) Eliminacion
        */
        [Test]
        public void Test18_Eliminar_Usuario_Admin_()
        {
          
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                eli.EliminacionUsuarioAdminBOClaro(driver, lista);
            
          
        }

    }
}