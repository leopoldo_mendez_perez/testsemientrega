﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test13_Validación_Campos_Vácios_Admin
    {

        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
        static login login = new login();
        ValidarCamposVacios_Administrador v = new ValidarCamposVacios_Administrador();
        /*
        CP01-12	Crear usuario(Administrador) Validacion de campos vacios (user)
        */
        [Test]
        public void Test13_Validación_Campos_Vácios_Admin_()
        {
            
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                v.altaUsuarioBOClaroValidacionCamposVacios(driver, lista, LocalizadoresBO._011101_linkAdministradores);
            
        }
    }
}