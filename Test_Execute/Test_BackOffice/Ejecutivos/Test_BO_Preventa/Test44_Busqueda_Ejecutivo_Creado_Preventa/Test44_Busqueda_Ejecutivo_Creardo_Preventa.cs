﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    #pragma warning disable CS1591
    public class Test44_Busqueda_Ejecutivo_Creardo_Preventa
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        private static List<String> lista = new List<String>();
        static login login = new login();

        Busqueda_Ejecutivo_Creado_Preventa adm = new Busqueda_Ejecutivo_Creado_Preventa();

        /*
        * CP01-44	buscar usuario (Administrador) busqueda de usuario Creado
        */

        [Test]
        public void Test44_Busqueda_Ejecutivo_Creado_Preventa()
        {
          
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                adm.BusquedaEjecutivoCreadoPreventa(driver, lista, xpath:LocalizadoresBO._014401_opcEjecutivosPreventa);

            
        }

    }
}
