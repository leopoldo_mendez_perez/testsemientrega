﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    #pragma warning disable CS1591
    public class  Test43_Crear_Ejecutivo_Preventa
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Funtions = new Selenium();
        private static List<String> lista = new List<String>();
        static login login = new login();
        Crear_Ejecutivo_Preventa prev = new Crear_Ejecutivo_Preventa();
        /*
         CP01-43 Crear ejecutivo de preventa 
        */
        [Test]
        public void Test43_Crear_Ejecutivo_Preventa_()
        {
          
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                prev.crearEjecutivoPreventaBO(driver, lista);
            
        }
    }
}
