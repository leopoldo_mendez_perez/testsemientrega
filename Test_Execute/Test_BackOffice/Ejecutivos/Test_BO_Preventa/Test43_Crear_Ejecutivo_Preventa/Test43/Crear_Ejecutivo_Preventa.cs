﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Threading;
using Test_Execute.Selenium.ResxBO;

namespace Test_Execute.Selenium.suit
{
    class Crear_Ejecutivo_Preventa
    {
        Selenium Functions = new Selenium();

        /*
* Fecha: 22.03.22
* Creador: Orlando López Brenes (QA)
* Objetivo: Alta de usuario Empresa 
* Ultima actualización:22/03/2022
* Responsable Ultima actualización:
* Objetivo ultima actualización:
*/
        public void crearEjecutivoPreventaBO(ChromeDriver driver, List<String> lista)
        {
            try
            {
                Functions.ClickButton(driver, "XPath", LocalizadoresBO._014301_opcEjecutivos, lista, "0", 10);
                Functions.cargando(driver, LocalizadoresBO._011005_procesando);
                Functions.ClickButton(driver, "XPath", LocalizadoresBO._014302_btnCrearEjecutivoPreventa, lista, "0", 5);
                Functions.cargando(driver, LocalizadoresBO._011005_procesando);

                //Selección de opción PREVENTA
                Functions.ClickButton(driver, "XPath", LocalizadoresBO._014303_tipoEjecutivo, lista, "0", 10);
                Functions.ClickButton(driver, "XPath", LocalizadoresBO._014304_opcEjecutivoPreventa, lista, "0", 10);

                Functions.SetTextVal(driver, "XPath", LocalizadoresBO._014305_campoNombre, $"{DatosBO._014301_nombreEjecutivoPreventa}", lista, "0");
                var jsonFolder = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @".\\");
                //SP significa Clase super Padre
                var pathFinal = jsonFolder.Replace("bin\\Debug\\.\\", "") + "Imag\\ejecutivo.png";
                Functions.SetTextVal(driver, "XPath", LocalizadoresBO._014311_inputFile, pathFinal, lista, "0");
                Functions.scrooll(driver, LocalizadoresBO._014302_btnCrearEjecutivoPreventa);
                Functions.SetTextVal(driver, "XPath", LocalizadoresBO._014307_campoCorreo, $"{DatosBO._014302_email}", lista, "0");
                Functions.SetTextVal(driver, "XPath", LocalizadoresBO._014308_campoTelefono, $"{DatosBO._014303_Telefono}", lista, "0");

                Functions.ClickButton(driver, "XPath", LocalizadoresBO._014309_btnCrearEjecutivoPreventa, lista, "0", 10);
                Functions.cargando(driver, LocalizadoresBO._011005_procesando);
                Thread.Sleep(10000);
                Functions.ExistWhitParameter(driver,LocalizadoresBO._014312_txtConfirmaciónCrearEjecutivo, 10);
                

            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error" + e);

            }
            Thread.Sleep(10000);
            driver.Quit();
        }
    }

}
