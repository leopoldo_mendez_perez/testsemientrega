﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;
using Test_Inactivos;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test23_Asociación_Lineas_Empresa
    {
        //////////////////////////////////////////////////SELENIUM///////////////
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
      
        Asociación_Lineas_Empresa aso = new Asociación_Lineas_Empresa();
        static login login = new login();
        Lectura_Test_Inactivos lecEstado = new Lectura_Test_Inactivos();

        /*
        * CP01-26	Usuario (Empresa) Asociación de linea
        */
        [Test]
#pragma warning disable CS1591
        public void Test23_Asociación_Lineas_Empresa_()
        {
            if (!lecEstado.LecturaTestInactivos("Asociación_Lineas_Empresa"))
            {
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                aso.AsociaciónLineaEmpresaBOClaroreado(driver, lista, LocalizadoresBO._013201_opciónBusqEmpresa, LocalizadoresBO._013903_textVadi, DatosBO._013805_adminSigP1, DatosBO._011902_usuarioAdminEmpresa, "Empresa");
            }
            else
            {
                Assert.Ignore("Test ignored during Prod runs");
            }
        }
    }
}