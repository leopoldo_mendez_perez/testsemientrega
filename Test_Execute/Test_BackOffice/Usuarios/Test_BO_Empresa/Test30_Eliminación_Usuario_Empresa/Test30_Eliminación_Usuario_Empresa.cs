﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;
using Test_Inactivos;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test30_Eliminar_Usuario_Empresa
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
        Lectura_Test_Inactivos lecEstado = new Lectura_Test_Inactivos();
        static login login = new login();
        Eliminación_Usuario_Empresa eli = new Eliminación_Usuario_Empresa();
        /*
        * CP01-30 Usuario (Empresa) Eliminacion
        */
        [Test]
        public void Test30_Eliminar_Usuario_Empresa_()
        {
            if (!lecEstado.LecturaTestInactivos("Eliminación_Usuario_Empresa"))
            {
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                eli.EliminacionUsuarioEmpresaBOClaro(driver, lista);
            }
            else
            {
                Assert.Ignore("Test ignored during Prod runs");
            }
        }

    }
}