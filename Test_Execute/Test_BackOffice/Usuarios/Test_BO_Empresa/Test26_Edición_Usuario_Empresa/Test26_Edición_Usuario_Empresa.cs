﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;
using Test_Inactivos;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test26_EdiciónUsuario_Empresa
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
        Lectura_Test_Inactivos lecEstado = new Lectura_Test_Inactivos();
        static login login = new login();
        Edición_Usuario_Empresa edU = new Edición_Usuario_Empresa();
        /*
         * CP01-22	Usuario (Empresa) Edicion de Usuario
         */
        [Test]
        public void Test26_EdiciónUsuario_Empresa_()
        {

            if (!lecEstado.LecturaTestInactivos("Edición_Usuario_Empresa"))
            {
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                edU.EdicionUsuarioEmpresaBOClaroreado(driver, lista, LocalizadoresBO._013201_opciónBusqEmpresa, LocalizadoresBO._013903_textVadi, DatosBO._013805_adminSigP1, DatosBO._011902_usuarioAdminEmpresa);
            }
            else
            {
                Assert.Ignore("Test ignored during Prod runs");
            }
        }

    }
}