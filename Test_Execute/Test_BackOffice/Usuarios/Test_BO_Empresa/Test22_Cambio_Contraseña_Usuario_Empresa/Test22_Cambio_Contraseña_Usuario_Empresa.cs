﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;
using Test_Inactivos;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test22_Cambio_Contraseña_Usuario_Empresa
    {

        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
      
        static login login = new login();
        Lectura_Test_Inactivos lecEstado = new Lectura_Test_Inactivos();
        Cambio_Contraseña_Usuario_Empresas c = new Cambio_Contraseña_Usuario_Empresas();
        /*
		* CP01-22Usuario (Empresa) Cambio de contraseña
		*/
        [Test]
#pragma warning disable CS1591
        public void Test22_Cambio_Contraseña_Usuario_Empresa_()
        {
            if (!lecEstado.LecturaTestInactivos("Cambio_Contraseña_Usuario_Empresa"))
            {

                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                c.CambioContraseñaUsuarioEmpresaBOClaroreado(driver, lista, LocalizadoresBO._013201_opciónBusqEmpresa, LocalizadoresBO._012205_textConfirm,DatosBO._012001_SigP1, DatosBO._011902_usuarioAdminEmpresa);
            }
            else
            {
                Assert.Ignore("Test ignored during Prod runs");
            }
        }

    }
}