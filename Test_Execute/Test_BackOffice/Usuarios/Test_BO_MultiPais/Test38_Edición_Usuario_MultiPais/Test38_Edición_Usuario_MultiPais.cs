﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test38_EdiciónUsuario_MultiPais
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
        static login login = new login();
        Edición_Usuario_MultiPais edU = new Edición_Usuario_MultiPais();
        /*
         * CP01-38	Usuario (MultiPais) Edicion de Usuario
         */
        [Test]
        public void Test38_EdiciónUsuario_MultiPais_()
        {

          
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
               edU.EdicionUsuarioEmpresaBOClaroreado(driver, lista, LocalizadoresBO._013201_opciónBusqEmpresa, LocalizadoresBO._013501_textoEvaluar, DatosBO._013805_adminSigP1, DatosBO._013102_usuarioAdmin);
            
        }

    }
}