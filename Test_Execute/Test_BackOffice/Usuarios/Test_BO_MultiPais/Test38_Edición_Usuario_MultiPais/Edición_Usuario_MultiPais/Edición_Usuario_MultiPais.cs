﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Test_Execute.Selenium.ResxBO;
using Test_Execute.Selenium.ResxConfGlob;
using OpenQA.Selenium.Support.UI;
using System.Reflection;
using System.Configuration;
using AventStack.ExtentReports;
using NUnit.Framework;

namespace Test_Execute.Selenium.suit
{
    class Edición_Usuario_MultiPais
    {

        Selenium Functions = new Selenium();
        string ruta = @"C:\Logs_TE_TER\";
       
        string Orden;
        static String[] part; // variable para almacenar las cadenas desintegradas

        /*
             * Fecha: 27.02.22
             * Creador: Leopoldo Mendez Perez (QA)
             * Objetivo: Metodo La Edición de usuario MultiPais
             * Ultima actualización:14-03-2022
             * Responsable Ultima actualización:Leopoldo Méndez Pérez (QA Sigel)
             * Objetivo ultima actualización:Incorporación nueva Mejora para la ejecución de test
             */
        public void EdicionUsuarioEmpresaBOClaroreado(ChromeDriver driver, List<string> lista, String xpath, String textoDeValidacion, String siglas, String usuarioEmpresa)
        {
            try { 
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 10);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", xpath, lista, "0", 10);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._010801_inputSearch, siglas + $"{usuarioEmpresa}" + DatosBO._012002_SigP2, lista, "0");
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010802_columFecha, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010901_buttonUserEdit, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010902_opcionEdit, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.clean(driver, "XPath", LocalizadoresBO._011908_usuarioAdmin, lista, "0");
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._011908_usuarioAdmin, $"{DatosBO._013801_usuarioNuevo}", lista, "0");
            Functions.scrooll(driver, LocalizadoresBO._010903_buttonGuardar);
            Functions.ClickButton(driver, "Value", LocalizadoresBO._010903_buttonGuardar, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.scrooll(driver, textoDeValidacion);
            Functions.ExistWhitParameter(driver, textoDeValidacion, 3);
            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            driver.Quit();
        }
    }
}
