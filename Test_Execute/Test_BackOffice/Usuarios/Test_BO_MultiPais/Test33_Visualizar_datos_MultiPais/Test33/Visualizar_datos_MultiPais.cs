﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Threading;
using Test_Execute.Selenium.ResxBO;


namespace Test_Execute.Selenium.suit
{
    class Visualización_Datos_MultiPais
    {

        Selenium Functions = new Selenium();
       
        /*
      * Fecha: 02.01.22
      * Creador: Leopoldo Mendez Perez (QA)
      * Objetivo: Metodo para la Visualización de datos de Usuario MultiPais
      * Ultima actualización:14-03-2022
      * Responsable Ultima actualización:Leopoldo Méndez Pérez (QA Sigel)
      * Objetivo ultima actualización:Incorporación nueva Mejora para la ejecución de test
      */
        public void VerDatosUsuarioEmpresaBOClaroreado(ChromeDriver driver, List<String> lista, String usuarioAdmin, String xpath, String siglas)
        {
            try { 
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 10);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", xpath, lista, "0", 0);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._010801_inputSearch, siglas + $"{usuarioAdmin}" + DatosBO._012002_SigP2, lista, "0");
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010901_buttonUserEdit, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._01211_opciónVerDatos, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011934_copiUser, lista, "0", 5);
            Functions.ExistWhitParameter(driver, LocalizadoresBO._011936_alertCopyUser, 10);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011935_copiPassword, lista, "0", 10);
            Functions.ExistWhitParameter(driver, LocalizadoresBO._011937_alertCopyPassword, 10);
            driver.Quit();
            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
        }
    }
}
