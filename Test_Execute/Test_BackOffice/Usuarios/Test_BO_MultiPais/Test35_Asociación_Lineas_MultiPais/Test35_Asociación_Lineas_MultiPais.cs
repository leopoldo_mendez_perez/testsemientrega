﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test35_Asociación_Lineas_MultiPais
    {
        //////////////////////////////////////////////////SELENIUM///////////////
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
      
        Asociación_Lineas_MultiPais aso = new Asociación_Lineas_MultiPais();
        static login login = new login();

        /*
        * CP01-35	Usuario (MultiPais) Asociación de linea
        */
        [Test]
        public void Test35_Asociación_Lineas_MultiPais_()
        {
            
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                aso.AsociaciónLineaEmpresaBOClaroreado(driver, lista, LocalizadoresBO._013201_opciónBusqEmpresa, LocalizadoresBO._013501_textoEvaluar, DatosBO._013805_adminSigP1, DatosBO._013102_usuarioAdmin, "MultiPaís");
           
        }
    }
}