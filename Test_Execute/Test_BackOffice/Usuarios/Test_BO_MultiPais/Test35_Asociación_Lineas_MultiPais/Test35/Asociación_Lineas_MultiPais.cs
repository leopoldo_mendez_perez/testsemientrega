﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Threading;
using Test_Execute.Selenium.ResxBO;


namespace Test_Execute.Selenium.suit
{
    class Asociación_Lineas_MultiPais
    {

        Selenium Functions = new Selenium();
       
        /*
      * Fecha: 02.01.22
      * Creador: Leopoldo Mendez Perez (QA)
      * Objetivo: Metodo para la asociacion de Linea
      * Ultima actualización:23-02-2022,14-03-2022
      * Responsable Ultima actualización:Leopoldo Méndez Pérez (QA Sigel)
      * Objetivo ultima actualización:Incorporación nueva Mejora para la ejecución de test
      */
        public void AsociaciónLineaEmpresaBOClaroreado(ChromeDriver driver, List<string> lista, String xpath, String textoDeValidación, String siglas, String usuarioMultiPaís, String tipo)
        {
            try { 
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 10);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", xpath, lista, "0", 10);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._010801_inputSearch, siglas + $"{usuarioMultiPaís}" + DatosBO._012002_SigP2, lista, "0");
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010802_columFecha, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010901_buttonUserEdit, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._012601_editarServicioAsociado, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Thread.Sleep(2000);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);

            if (tipo == "Empresa")
            {
                Functions.SetTextVal(driver, "XPath", LocalizadoresBO._011931_selectServicioInput, DatosBO._012301_servicioAsociar1, lista, "0");
                Functions.ClickButton(driver, "XPath", LocalizadoresBO._011932_asocServicio, lista, "0", 0);
                Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            }
            else if (tipo == "MultiPaís")
            {
                asociacionNumero(driver, lista, DatosBO._013107_númeroGTAgregar, "502");
            }
                    
            if (Functions.ExistElement(driver, "XPath", LocalizadoresBO._012301_LineaAsociada, lista, "0")) {
                Functions.ExistWhitParameter(driver,"La Linea Ya esta Asociada", 3);
            };
            Functions.ClickButton(driver, "Value", LocalizadoresBO._010903_buttonGuardar, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ExistWhitParameter(driver, textoDeValidación, 3);
            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            driver.Quit();
        }
        //funcion para asociar número 
        public void asociacionNumero(ChromeDriver driver, List<String> lista, String numero, String CodPais)
        {
            Functions.SendKeysTab(driver, LocalizadoresBO._013104_ingresoCodigoPais, $"{CodPais}");
            Functions.clean(driver, "XPath", LocalizadoresBO._013103_número, lista, "0");
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._013103_número, $"{numero}", lista, "0");
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011932_asocServicio, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);

        }
    }
}
