﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test37_Desasociación_Linea_MultiPais
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
        public static bool Test1Called;
        static login login = new login();
        Desasociación_Linea_MultiPais des = new Desasociación_Linea_MultiPais();

        /*
        * CP01-37 Usuario (MultiPais) Desasociación de linea
        */
        [Test]
        public void Test37_Desasociación_Linea_MultiPais_()
        {
            
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                des.DesasociaciónLineaEmpresaBOClaroreado(driver, lista, LocalizadoresBO._013201_opciónBusqEmpresa, LocalizadoresBO._013501_textoEvaluar, DatosBO._013805_adminSigP1, DatosBO._013102_usuarioAdmin);
           
        }
    }
}