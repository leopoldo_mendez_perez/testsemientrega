﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test36_Validación_Mensaje_Linea_Asociada_Usuario_MultiPais
    {
        //////////////////////////////////////////////////SELENIUM///////////////
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
        Validación_Mensaje_Linea_Asociada_MultiPais val = new Validación_Mensaje_Linea_Asociada_MultiPais();
        static login login = new login();

        /*
       * CP01-36 Usuario (MultiPais) Validación de Mensaje "Linea ya esta asociada"
       * */
        [Test]
        public void Test36_Validación_Mensaje_Linea_Asociada_Usuario_MultiPais_()
        {
           
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                val.ValidaciónLineaAsociadaEmpresaBOClaroCreado(driver, lista, LocalizadoresBO._013201_opciónBusqEmpresa, LocalizadoresBO._012302_textLineaAsociada, "MultiPaís", DatosBO._013805_adminSigP1, DatosBO._013102_usuarioAdmin);
       
        }
    }
}