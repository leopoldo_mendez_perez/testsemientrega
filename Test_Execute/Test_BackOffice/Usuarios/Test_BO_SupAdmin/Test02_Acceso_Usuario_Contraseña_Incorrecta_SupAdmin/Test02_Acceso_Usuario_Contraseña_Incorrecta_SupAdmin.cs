﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;
using Test_Execute.Selenium.suit;
using System.Threading;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test02_Acceso_Usuario_Contraseña_Incorrecta_SupAdmin
    {
      
        static ChromeDriver driver;
        static Login_Usuario_Contraseña_Incorrecta loginUserPassIncorrect = new Login_Usuario_Contraseña_Incorrecta();
            private static List<String> lista = new List<String>();
        static Config Conf = new Config();

        /*
        * CP01-02	Acceder al portal del BackOffice usuario y password incorrecto
        */
        [Test]
        public void Test02_Acceso_Usuario_Contraseña_Incorrecta_SupAdmin_()
        {
        
                driver = Conf.ConfigChrome();
                loginUserPassIncorrect.LoginUserPassowrdError(driver, lista, DatosBO._010201_userError, DatosBO._010202_passwordError);

           
        }
    }
}