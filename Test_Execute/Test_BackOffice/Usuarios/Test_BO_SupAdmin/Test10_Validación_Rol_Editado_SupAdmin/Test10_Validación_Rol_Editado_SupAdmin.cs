﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test10_Validación_Rol_Editado_SupAdmin
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        static ValidacionEdicionUsuarioBOClaroreado suit = new ValidacionEdicionUsuarioBOClaroreado();
        login login = new login();
        private static List<String> lista = new List<String>();
      
        /*
              * CP01-10	Validacion Editar Usuario (SuperAdministrador) Edicion de Rol
              */
        [Test]
        public void Test10_Validación_Rol_Editado_SupAdmin_()
        {
            
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                suit.ValidacionEdicionUsuarioBOClaroread(driver, lista, LocalizadoresBO._010908_opcionSuperAdministrador);
           
        }

    }
}