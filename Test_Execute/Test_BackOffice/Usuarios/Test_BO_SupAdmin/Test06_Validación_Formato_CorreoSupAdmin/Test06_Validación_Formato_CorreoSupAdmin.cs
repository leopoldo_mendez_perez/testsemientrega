﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test06_Validación_Formato_CorreoSupAdmin
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        static ValidarFormatoCorreo ValidCorreo = new ValidarFormatoCorreo();
        private static List<String> lista = new List<String>();
      
        string Orden; static login login = new login();
        /*
        * CP01-06	Crear usuario (SuperAdministrador) Validación de formato de correo
        */
        [Test]
        public void Test06_Validación_Formato_CorreoSupAdmin_()
        {
            driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                ValidCorreo.altaUsuarioBOClaroValidacionFormatoCorreo(driver, lista, LocalizadoresBO._010403_linkSuperAdministradores);
            
        }

    }
}