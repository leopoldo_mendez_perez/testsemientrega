﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;
namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test03_Acceso_Usuario_Incorrecto_SupAdmin
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        static Login_con_usuario_Incorrecto loginUsuarioIncorrecto = new Login_con_usuario_Incorrecto();
        private static List<String> lista = new List<String>();
        /*
        * CP01-03	Acceder al portal del BackOffice usuario incorrecto
        */
        [Test]
        public void Test03_Acceso_Usuario_Incorrecto_SupAdmin_()
        {
          
                driver = Conf.ConfigChrome();
                loginUsuarioIncorrecto.LoginUserError(driver, lista, DatosBO._010201_userError, DatosBO._010202_passwordError);
                driver.Quit();
           
        }
    }
}