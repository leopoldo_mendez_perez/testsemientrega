﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Test_Execute.Selenium.ResxBO;
using Test_Execute.Selenium.ResxConfGlob;
using OpenQA.Selenium.Support.UI;
using System.Reflection;
using System.Configuration;
using AventStack.ExtentReports;
using NUnit.Framework;

namespace Test_Execute.Selenium.suit
{
    class BusquedaUsuarioCreado
    {

        Selenium Functions = new Selenium();
        string ruta = @"C:\Logs_TE_TER\";
       
        string Orden;

        /*
       * Fecha:21.02.22
       * Creador: Leopoldo Méndez Pérez (QA)
       * Objetivo: Método para de busqueda de usuario SupAdministrador Creado
       * Ultima actualización:
       * Responsable Ultima actualización:
       * Objetivo ultima actualización:
       */
        public void BusquedaUsuarioBOClaroreado(ChromeDriver driver, List<String> lista, String usuarioAdmin, String xpath, String siglas)
        {
            try { 
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 10);
            Functions.ClickButton(driver, "XPath", xpath, lista, "0", 0);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._010801_inputSearch, siglas + $"{usuarioAdmin}" + DatosBO._011202_Admin_SupAdmSigP2, lista, "0");
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010802_columFecha, lista, "0", 0);
            Functions.ExistWhitParameter(driver, LocalizadoresBO._010803_validUserP1 + siglas + usuarioAdmin + DatosBO._011202_Admin_SupAdmSigP2 + LocalizadoresBO._010803_validUserP2, 3);
                // Console.WriteLine("Prueba ejecutada Exitosamente");
            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            Thread.Sleep(10000);
            driver.Quit();
        }
    }
}
