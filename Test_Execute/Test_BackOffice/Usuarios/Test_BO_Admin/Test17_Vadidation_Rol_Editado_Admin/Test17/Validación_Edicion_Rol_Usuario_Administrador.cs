﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Test_Execute.Selenium.ResxBO;
using Test_Execute.Selenium.ResxConfGlob;
using OpenQA.Selenium.Support.UI;
using System.Reflection;
using System.Configuration;
using AventStack.ExtentReports;
using NUnit.Framework;

namespace Test_Execute.Selenium.suit
{
    class ValidacionEdicionUsuarioAdministrador
    {

        Selenium Functions = new Selenium();
        string ruta = @"C:\Logs_TE_TER\";
       
        string Orden;
        static String[] part; // variable para almacenar las cadenas desintegradas
        /*
        * Fecha: 23.02.22
        * Creador: Leopoldo Mendez Perez (QA)
        * Objetivo: Método para la validación de los roles editados
        * Ultima actualización:
        * Responsable Ultima actualización:
        * Objetivo ultima actualización:
        */
        public void ValidacionEdicionUsuarioBOClarocreado (ChromeDriver driver, List<String> lista, String usuarioAdmin, String xpath, String siglas)
        {
            try { 
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 10);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", xpath, lista, "0", 10);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._010801_inputSearch, siglas + $"{usuarioAdmin}" + DatosBO._011202_Admin_SupAdmSigP2, lista, "0");
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010802_columFecha, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010901_buttonUserEdit, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010902_opcionEdit, lista, "0", 0);
            Functions.scrooll(driver, LocalizadoresBO._010407_radioButtonPregFre);
            part = Functions.SeparaTexto(DatosBO._011601_Rol_Usuario_Administrador);
            String numerofin, textofin;
            for (int i = 0; i < part.Length; i++)
            {
                //Lineas de codigo para extraer el estado del rol y tipo de Rol;

                textofin = (part[i].Substring(2)).Replace(" ", "");
                if (i == 0)
                {
                    numerofin = (part[i].Substring(0, 1)).Replace(" ", "");
                }
                else
                {
                    numerofin = (part[i].Substring(1, 1)).Replace(" ", "");
                }
                //---------------------------------------
                //ROL BIBLIOTECA
                if (numerofin == "1" && textofin == "Preguntasfrecuentes")
                {
                    Functions.ExistWhitParameter(driver,LocalizadoresBO._011006_Rol_EditadoP1+ "Preguntas frecuentes"+ LocalizadoresBO._011007_Rol_EditadoP2, 3);
                    Console.WriteLine("Validación de Rol Editado: "+textofin);
                }
                //ROL BIBLIOTECA
                if (numerofin == "1" && textofin == "Biblioteca")
                {

                    Functions.ExistWhitParameter(driver, LocalizadoresBO._011006_Rol_EditadoP1 + textofin + LocalizadoresBO._011007_Rol_EditadoP2, 3);
                    Console.WriteLine("Validación de Rol Editado: " + textofin);
                }
                //ROL TIENDA
                if (numerofin == "1" && textofin == "Tiendas")
                {

                    Functions.ExistWhitParameter(driver, LocalizadoresBO._011006_Rol_EditadoP1 + textofin + LocalizadoresBO._011007_Rol_EditadoP2, 3);
                    Console.WriteLine("Validación de Rol Editado: " + textofin);
                }
                //ROL USUARIO
                if (numerofin == "1" && textofin == "Usuarios")
                {

                    Functions.ExistWhitParameter(driver, LocalizadoresBO._011006_Rol_EditadoP1 + textofin + LocalizadoresBO._011007_Rol_EditadoP2, 3);
                    Console.WriteLine("Validación de Rol Editado: " + textofin);
                }
                //ROL Ejecutivos
                if (numerofin == "1" && textofin == "Ejecutivos")
                {

                    Functions.ExistWhitParameter(driver, LocalizadoresBO._011006_Rol_EditadoP1 + textofin + LocalizadoresBO._011007_Rol_EditadoP2, 3);
                    Console.WriteLine("Validación de Rol Editado: " + textofin);
                }

                //ROL Banners
                if (numerofin == "1" && textofin == "Banners")
                {
                    Functions.ExistWhitParameter(driver, LocalizadoresBO._011006_Rol_EditadoP1 + textofin + LocalizadoresBO._011007_Rol_EditadoP2, 3);
                    Console.WriteLine("Validación de Rol Editado: " + textofin);

                }
                //ROL Historial
                if (numerofin == "1" && textofin == "Historial")
                {
                    Functions.ExistWhitParameter(driver, LocalizadoresBO._011006_Rol_EditadoP1 + textofin + LocalizadoresBO._011007_Rol_EditadoP2, 3);
                    Console.WriteLine("Validación de Rol Editado: " + textofin);

                }


            }

            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            driver.Quit();
        }
    }
}
