﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;
using Test_Inactivos;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
#pragma warning disable CS1591
    public class Test16_Edit_Rol_User_Admi
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
        static login login = new login();
        Edicion_Rol_Usuario_Administrador ed = new Edicion_Rol_Usuario_Administrador();
        Lectura_Test_Inactivos lecEstado = new Lectura_Test_Inactivos();

        [Test]
        public void Test16_Edit_Rol_User_Admi_()
        {
            if (!lecEstado.LecturaTestInactivos("Editar_Rol_Useruario_Admin"))
            {
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                ed.EdicionUsuarioBOClaroreado(driver, lista, LocalizadoresBO._011601_opcionAdministrador, LocalizadoresBO._011602_userAdminValidatedText1);
            }
            else
            {
                Assert.Ignore("Test ignored during Prod runs");
            }
        }

    }
}