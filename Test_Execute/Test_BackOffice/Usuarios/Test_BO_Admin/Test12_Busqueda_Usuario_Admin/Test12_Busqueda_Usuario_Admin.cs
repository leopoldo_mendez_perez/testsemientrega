﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using Test_Inactivos;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
#pragma warning disable CS1591
    public class Test12_Buscar_Usuario_Creado_Administrador
    {

        //////////////////////////////////////////////////SELENIUM///////////////
        static ChromeDriver driver;
        static Config Conf = new Config();
        private static List<String> lista = new List<String>();
        static login login = new login();
        Lectura_Test_Inactivos lecEstado = new Lectura_Test_Inactivos();

        Busqueda_Usuario_Creado_Administrador adm = new Busqueda_Usuario_Creado_Administrador();
        /*
        * CP01-12	buscar usuario (Administrador) busqueda de usuario Creado
        */
        [Test]
        public void Test12_Buscar_Usuario_Creado_Administrador_()
        {

            if (!lecEstado.LecturaTestInactivos("Busqueda_Usuario_Admin"))
            {
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                adm.BusquedaUsuarioBOClarocreado(driver, lista, DatosBO._011101_usuarioAdministrador, LocalizadoresBO._011601_opcionAdministrador, DatosBO._011201_AdminSigP1);
            }
            else
            {
                Assert.Ignore("Test ignored during Prod runs");
            }
        }

    }
}