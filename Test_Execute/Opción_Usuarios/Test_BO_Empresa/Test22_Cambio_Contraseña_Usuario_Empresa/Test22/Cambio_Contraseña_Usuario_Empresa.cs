﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Threading;
using Test_Execute.Selenium.ResxBO;


namespace Test_Execute.Selenium.suit
{
    class Cambio_Contraseña_Usuario_Empresas
    {

        Selenium Functions = new Selenium();
       
        /*
      * Fecha: 02.01.22
      * Creador: Leopoldo Mendez Perez (QA)
      * Objetivo: Metodo para el cambio de contraseña del usuario Empresa
      * Ultima actualización:23-02-2022
      * Responsable Ultima actualización:
      * Objetivo ultima actualización:
      */

        public void CambioContraseñaUsuarioEmpresaBOClaroreado(ChromeDriver driver, List<string> lista, String xpath, String textoDeValidación,String siglas,String usuarioAdmin)
        {
            try { 
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 10);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", xpath, lista, "0", 10);
            Thread.Sleep(2000);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._010801_inputSearch, siglas + $"{usuarioAdmin}" + DatosBO._012002_SigP2, lista, "0");
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010802_columFecha, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010901_buttonUserEdit, lista, "0", 0);
            Functions.scrooll(driver, LocalizadoresBO._012201_opcionCambioContraseña);

            Functions.ClickButton(driver, "XPath", LocalizadoresBO._012201_opcionCambioContraseña, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Thread.Sleep(2000);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._012202_inputNuevaContraseña, $"{DatosBO._012201_nuevaContraseña}", lista, "0");
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._012203_inputConfirmarContraseña, $"{DatosBO._012202_confirmarContraseña}", lista, "0");

            Functions.ClickButton(driver, "Value", LocalizadoresBO._012204_buttonConfirm, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ExistWhitParameter(driver, textoDeValidación, 3);
            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            driver.Quit();
        }
    }
}
