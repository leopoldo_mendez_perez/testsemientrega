﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test28_Edición_Rol_Usuario_Empresa
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
        Edicion_Rol_Usuario_Empresa edi = new Edicion_Rol_Usuario_Empresa();
        static login login = new login();

        /*
        * CP01-28	Usuario (Empresa) Edicion de Rol
        */
        [Test]
        public void Test28_Edición_Rol_Usuario_Empresa_()
        {
          
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                edi.EdicionRolUsuarioEmpresa(driver, lista, LocalizadoresBO._013201_opciónBusqEmpresa, LocalizadoresBO._013903_textVadi, LocalizadoresBO._013201_opciónBusqEmpresa);
          

        }
    }
}