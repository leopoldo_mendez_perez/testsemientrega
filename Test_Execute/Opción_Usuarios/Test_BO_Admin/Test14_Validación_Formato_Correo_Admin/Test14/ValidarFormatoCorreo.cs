﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using Test_Execute.Selenium.ResxBO;



namespace Test_Execute.Selenium.suit
{
    class ValidarFormatoCorreoAdministrador
    {

        Selenium Functions = new Selenium();
        string ruta = @"C:\Logs_TE_TER\";
       
        string Orden;


        /*
        * 
        * Creador: Ruben Velazquez Patiño (bSide) Fecha: 02.11.21
        * Modificado: Leopoldo Mendez Perez (QA)  Fecha: 22.02.22
        * Objetivo: Método para validación del formato del correo en el formulario de alta de usuario para BO para clientes
        * Ultima actualización:
        * Responsable Última actualización:
        * Objetivo última actualización:
        */
        public void altaUsuarioBOClaroValidacionFormatoCorreo(ChromeDriver driver, List<String> lista, String xpath)
        {
            try { 
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 5);
            Functions.scrooll(driver, LocalizadoresBO._010404_buttonCrearUsuario);
            Functions.ClickButton(driver, "Id", LocalizadoresBO._010402_buttonList, lista, "0", 10);
            Functions.ClickButton(driver, "XPath", xpath, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._010406_correo_Electronico, $"{"uate31"}", lista, "0");
            Functions.scrooll(driver, LocalizadoresBO._010414_buttonCreateUser);
            Functions.ClickButton(driver, "Value", LocalizadoresBO._010414_buttonCreateUser, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.Equals(driver, LocalizadoresBO._010601_email_error_text, DatosBO._010601_email_format_error_text);
            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            driver.Quit();
        }
    }
}
