﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Threading;
using Test_Execute.Selenium.ResxBO;


namespace Test_Execute.Selenium.suit
{
    class Crear_Usuario_Admin
    {

        Selenium Functions = new Selenium();
        

        /*
* Fecha: 23.12.21
* Creador: Leopoldo Mendez Perez (QA)
* Objetivo: Método para la creación de usuario Administrador
* Ultima actualización:
* Responsable Ultima actualización:
* Objetivo ultima actualización:
*/
        public void altaUsuarioAdministradorBOClaro(ChromeDriver driver, List<String> lista, String usuarioAdmin, String correoAdmin)
        {
            try { 
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 10);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.scrooll(driver, LocalizadoresBO._010404_buttonCrearUsuario);
            Functions.ClickButton(driver, "Id", LocalizadoresBO._010402_buttonList, lista, "0", 0);

            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011101_linkAdministradores, lista, "0", 0);

            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._010405_nombre_Usuario, $"{usuarioAdmin}", lista, "0");
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._010406_correo_Electronico, $"{correoAdmin}", lista, "0");
            //Seleccion de Rol de superAdministrador
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010407_radioButtonPregFre, lista, "0", 0);
            Functions.scrooll(driver, LocalizadoresBO._010408_radioButtonBibl);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010408_radioButtonBibl, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010409_radioButtonTien, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010410_radioButtonUsu, lista, "0", 0);
            Functions.scrooll(driver, LocalizadoresBO._010410_radioButtonUsu);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010411_radioButtonEje, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010412_radioButtonBan, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010413_radioButtonHist, lista, "0", 0);
            Functions.ClickButton(driver, "Value", LocalizadoresBO._010414_buttonCreateUser, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ExistWhitParameter(driver, LocalizadoresBO._011102_ValidacionCreacionUsuarioAdmin, 3);
            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            driver.Quit();
        }




    }
}
